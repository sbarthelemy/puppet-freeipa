# puppet-freeipa

## 2.1.0 :

  * remove last string facts by structured facts  : #48, #43
  * use datatype Stdlib::IP::Address              : #47
  * clean up puppetlabs-stdlib requirement        : #44
  * add custom fact giving configured ipa role    : #18

## 2.0.1 :

  * clean up REFERENCES.md                        : #42
  * clean up domain name in tests and README      : #41
  * fix metadata.json                             : #39

## 2.0.0 :

  * pin beaker-vagrant to version 0.5.0           : #36
  * remove selinux from module code               : #32
  * enable acceptance tests                       : #35, #34, #33, #31, #29, #26, #24, #22, #20
  * enable puppet datatype                        : #19
  * add licence file                              : #30
  * configure epel with module stahnma-epel       : #11
  * unit tests with rspec-puppet-facts            : #15
  * use pdk to receive guidance                   : #5
  * enable more rubucop cops                      : #17
  * rename classes from `easy_ipa` to `freeipa`   : #2
  * fix installation of master                    : #3
  * finx installation of replica                  : #4

## 1.6.1 :

  * First release under `adullact` name space     : #1

